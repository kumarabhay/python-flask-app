import json
import pytest
import logging
from helloapi2 import helloapi2

@pytest.fixture
def client(request):
    test_client = helloapi2.test_client()

    def teardown():
        pass # databases and resourses have to be freed at the end. But so far we don't have anything

    request.addfinalizer(teardown)
    return test_client

def test_getrequest(client):
    response = client.get('/api/helloapi2/test')
    assert b'Hello from Python Rest API 2' in response.data