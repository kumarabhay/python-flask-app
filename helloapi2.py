import logging
import os
from flask import Flask, request, Response, json

logging.basicConfig(level=logging.INFO)

helloapi2 = Flask(__name__)


@helloapi2.route("/api/helloapi2/test")
def hello():
    logging.info("Started hello rest api2")
    return "Hello from Python Rest API 2"


if __name__ == "__main__":
    helloapi2.run(debug=True, host='0.0.0.0', port='5001')

